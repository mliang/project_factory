## project_factory

#### 使用

1. 安装依赖

```
npm i
```

当前只是用到了lodash，PS：模板文件内的替换主要使用了lodash的template函数。

2. 运行

```
node project.js
```

project.js是主体代码，通过读取项目结构配置文件project.json，生成项目结构和项目代码。

#### project.json [项目结构配置]

> catalog节点下为项目目录结构，其中Object为文件夹，Array为文件，文件生成规则为数组中名称+前缀

> project.json数据可以在模板文件中读取

> 模板下文件对应规则：模板文件 = catalog下目录路径连接+文件前缀，优先查找当前目录名称对应的模板文件，如果没有依次向上查找

```
{
  "author": "mliang", 
  "appName": "app",
  "template": "template/", //必须存在，指定模板文件目录
  "catalog":  //必须存在，该节点下内容为项目目录
  {
    "app":
    {
      "assets":
      {
        "css":
        {
          ".css": ["app", "style"],
          ".html": ["components.demo"]
        }
      },
      "modules":
      {
        "demo":
        {
          ".css": ["demo"],
          ".controller.js": ["demo"],
          ".service.js": ["demo"],
          ".html": ["demo_list", "demo_info", "demo_view"]
        }
      }
    }
  }
}
```

#### template [模板代码]

> 模板代码位于template文件夹下面，也可以通过修改project.json中template值自定义路径

> 当前提供的模板代码是基于angular1+styleguide和我自己总结出来的模块划分方法

> 可以编写其他前端框架的模板，只要符合lodash.template函数的语法

```
(function() {
  /**
   * file: <%=fileName%>.controller.js
   * create_date: <%=current_date%>
   * author: <%=projectJson.author%>
   */

  angular.module('<%=projectJson.appName%>.<%=fileName%>', [])
    .config(['$stateProvider', <%=_.upperFirst(fileName)%>Config])<% _.forEach(_.get(projectJson.catalog, _.join(pathArr, '.'))['.html'], function(html) { %>
    .controller('<%= _.upperFirst(_.camelCase(html)) %>Controller', ['<%=_.upperFirst(fileName)%>Service', <%= _.upperFirst(_.camelCase(html)) %>Controller])<% }) %>

  function <%=_.upperFirst(fileName)%>Config($stateProvider) {
    $stateProvider
      .state('main.<%=fileName%>', {
        url: '/<%=fileName%>',
        template: '<div class="<%=fileName%>" ui-view="<%=fileName%>"></div>'
      })<% _.forEach(_.get(projectJson.catalog, _.join(pathArr, '.'))['.html'], function(html) { %>
      .state('main.<%=fileName%>.<%= _.last(_.words(html)) %>', {
        url: '/<%= _.last(_.words(html)) %>',
        views: {
          '<%=fileName%>': {
            templateUrl: 'modules/<%=fileName%>/<%=html%>.html',
            controller: '<%= _.upperFirst(_.camelCase(html)) %>Controller',
            controllerAs: 'vm'
          }
        }
      })<% }); %>
  }

<% _.forEach(_.get(projectJson.catalog, _.join(pathArr, '.'))['.html'], function(html) { %>
  function <%= _.upperFirst(_.camelCase(html)) %>Controller(<%=_.upperFirst(fileName)%>Service) {
    var vm = this;


    activate();

    ///////////////////

    function activate() {

    }
  }
<% }); %>
})();
```

