
(function() {
  'use strict';

  /**
   * file: <%=fileName%>.js
   * create_date: <%=current_date%>
   * author: <%=projectJson.author%>
   */


  angular.module('<%=projectJson.appName%>', [
    'ui.router',
  ]);

  angular.module('<%=projectJson.appName%>')
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', AppConfig])
    .controller('RootController', ['$rootScope', '$scope', RootController]);

  function AppConfig($stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';

    $urlRouterProvider.otherwise('/');
  }

  function RootController($rootScope, $scope) {
    $rootScope.$on('global.events', function(evt, eventName, params) {
      $rootScope.$broadcast('events.' + eventName, params);
    });
  }
})();