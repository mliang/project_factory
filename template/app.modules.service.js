(function() {
  'use strict';
  /**
   * file: <%=fileName%>.service.js
   * create_date: <%=current_date%>
   * author: <%=projectJson.author%>
   */

  angular.module('<%=projectJson.appName%>.<%=fileName%>')
    .factory('<%=_.upperFirst(fileName)%>Service', ['$q', 'CommonService', <%=_.upperFirst(fileName)%>Service]);

  function <%=_.upperFirst(fileName)%>Service($q, CommonService) {

    var service = {
      get<%=_.upperFirst(fileName)%>List: get<%=_.upperFirst(fileName)%>List,
    };

    return service;

    //////////////////


    function get<%=_.upperFirst(fileName)%>List(data) {
      return CommonService.get('/<%=fileName%>/list', data);
    }

    function get<%=_.upperFirst(fileName)%>Info(id) {
      return CommonService.get('/<%=fileName%>/info/' + id);
    }

    function delete<%=_.upperFirst(fileName)%>(id) {
      return CommonService.delete('/<%=fileName%>/info/' + id);
    }

    function save<%=_.upperFirst(fileName)%>(data) {
      return CommonService.post('/<%=fileName%>/info', data);
    }

    function update<%=_.upperFirst(fileName)%>(id, data) {
      return CommonService.put('/<%=fileName%>/info/' + id, data);
    }
  }

})();