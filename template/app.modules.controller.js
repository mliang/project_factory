(function() {
  /**
   * file: <%=fileName%>.controller.js
   * create_date: <%=current_date%>
   * author: <%=projectJson.author%>
   */

  angular.module('<%=projectJson.appName%>.<%=fileName%>', [])
    .config(['$stateProvider', <%=_.upperFirst(fileName)%>Config])<% _.forEach(_.get(projectJson.catalog, _.join(pathArr, '.'))['.html'], function(html) { %>
    .controller('<%= _.upperFirst(_.camelCase(html)) %>Controller', ['<%=_.upperFirst(fileName)%>Service', <%= _.upperFirst(_.camelCase(html)) %>Controller])<% }) %>

  function <%=_.upperFirst(fileName)%>Config($stateProvider) {
    $stateProvider
      .state('main.<%=fileName%>', {
        url: '/<%=fileName%>',
        template: '<div class="<%=fileName%>_module" ui-view="<%=fileName%>"></div>'
      })<% _.forEach(_.get(projectJson.catalog, _.join(pathArr, '.'))['.html'], function(html) { %>
      .state('main.<%=fileName%>.<%= _.last(_.words(html)) %>', {
        url: '/<%= _.last(_.words(html)) %>',
        views: {
          '<%=fileName%>': {
            templateUrl: 'modules/<%=fileName%>/<%=html%>.html',
            controller: '<%= _.upperFirst(_.camelCase(html)) %>Controller',
            controllerAs: 'vm'
          }
        }
      })<% }); %>
  }

<% _.forEach(_.get(projectJson.catalog, _.join(pathArr, '.'))['.html'], function(html) { %>
  function <%= _.upperFirst(_.camelCase(html)) %>Controller(<%=_.upperFirst(fileName)%>Service) {
    var vm = this;


    activate();

    ///////////////////

    function activate() {

    }
  }
<% }); %>
})();