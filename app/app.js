
(function() {
  'use strict';

  /**
   * file: app.js
   * create_date: Mon Aug 20 2018 10:25:06 GMT+0800 (中国标准时间)
   * author: maliang
   */


  angular.module('app', [
    'ui.router',
  ]);

  angular.module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', AppConfig])
    .controller('RootController', ['$rootScope', '$scope', RootController]);

  function AppConfig($stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';

    $urlRouterProvider.otherwise('/');
  }

  function RootController($rootScope, $scope) {
    $rootScope.$on('global.events', function(evt, eventName, params) {
      $rootScope.$broadcast('events.' + eventName, params);
    });
  }
})();