(function() {
  /**
   * file: menu.controller.js
   * create_date: Mon Aug 06 2018 20:08:48 GMT+0800 (中国标准时间)
   * author: maliang
   */

  angular.module('app.menu', [])
    .config(['$stateProvider', MenuConfig])
    .controller('MenuController', ['MenuService', MenuController])

  function MenuConfig($stateProvider) {
    $stateProvider
      .state('main.menu', {
        url: '/menu',
        template: '<div class="menu" ui-view="menu"></div>'
      })
      .state('main.menu.menu', {
        url: '/menu',
        views: {
          'menu': {
            templateUrl: 'modules/menu/menu.html',
            controller: 'MenuController',
            controllerAs: 'vm'
          }
        }
      })
  }


  function MenuController(MenuService) {
    var vm = this;


    activate();

    ///////////////////

    function activate() {

    }
  }

})();