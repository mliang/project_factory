(function() {
  'use strict';
  /**
   * file: consumer.service.js
   * create_date: Mon Aug 06 2018 20:07:20 GMT+0800 (中国标准时间)
   * author: maliang
   */

  angular.module('app.consumer')
    .factory('ConsumerService', ['$q', 'CommonService', ConsumerService]);

  function ConsumerService($q, CommonService) {

    var service = {
      getConsumerList: getConsumerList,
    };

    return service;

    //////////////////


    function getConsumerList(data) {
      return CommonService.get('/consumer/list', data);
    }

    function getConsumerInfo(id) {
      return CommonService.get('/consumer/info/' + id);
    }

    function deleteConsumer(id) {
      return CommonService.delete('/consumer/info/' + id);
    }

    function saveConsumer(data) {
      return CommonService.post('/consumer/info', data);
    }

    function updateConsumer(id, data) {
      return CommonService.put('/consumer/info/' + id, data);
    }
  }

})();