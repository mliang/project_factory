(function() {
  'use strict';
  /**
   * file: session.service.js
   * create_date: Mon Aug 06 2018 20:07:20 GMT+0800 (中国标准时间)
   * author: maliang
   */

  angular.module('app.session')
    .factory('SessionService', ['$q', 'CommonService', SessionService]);

  function SessionService($q, CommonService) {

    var service = {
      getSessionList: getSessionList,
    };

    return service;

    //////////////////


    function getSessionList(data) {
      return CommonService.get('/session/list', data);
    }

    function getSessionInfo(id) {
      return CommonService.get('/session/info/' + id);
    }

    function deleteSession(id) {
      return CommonService.delete('/session/info/' + id);
    }

    function saveSession(data) {
      return CommonService.post('/session/info', data);
    }

    function updateSession(id, data) {
      return CommonService.put('/session/info/' + id, data);
    }
  }

})();