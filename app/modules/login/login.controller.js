(function() {
  /**
   * file: login.controller.js
   * create_date: Mon Aug 06 2018 20:08:48 GMT+0800 (中国标准时间)
   * author: maliang
   */

  angular.module('app.login', [])
    .config(['$stateProvider', LoginConfig])
    .controller('LoginController', ['LoginService', LoginController])

  function LoginConfig($stateProvider) {
    $stateProvider
      .state('main.login', {
        url: '/login',
        template: '<div class="login" ui-view="login"></div>'
      })
      .state('main.login.login', {
        url: '/login',
        views: {
          'login': {
            templateUrl: 'modules/login/login.html',
            controller: 'LoginController',
            controllerAs: 'vm'
          }
        }
      })
  }


  function LoginController(LoginService) {
    var vm = this;


    activate();

    ///////////////////

    function activate() {

    }
  }

})();